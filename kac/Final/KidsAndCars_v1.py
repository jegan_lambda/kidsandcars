#!/usr/bin/env python
# coding: utf-8

# In[89]:


import docx
import re
import spacy
from spacy import displacy
from IPython.display import HTML, display
import pandas as pd
import json
import os
import pandas as pd
import datetime
import textract
import docx2txt
from spacy.matcher import Matcher
from word2number import w2n
from spacy.lang.en.stop_words import STOP_WORDS
nlp = spacy.load("en_core_web_sm")
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize, sent_tokenize


# In[49]:


docText = docx2txt.process(r"/mnt/mydata/ML/kidsandcars/Docs/KCDE00053 SV OT 7 20 16 Fatal Jackson.docx")
docText = docText.replace("-"," ")


# In[138]:


color = pd.read_csv("colors.csv")
loct = pd.read_excel("LOCKTWO.xlsx")
cars = pd.read_excel("carsnlp.xlsx")
county = pd.read_excel("county.xlsx")
mvown = pd.read_excel("MVOWN.xlsx")
pairs = dict(zip(list(loct["Locktwo"].apply(lambda x:x.lower())),[space.strip() for space in list(loct["LOCKTWO"])]))
own = dict(zip(list(mvown["OWN"].apply(lambda x:x.lower())),list(mvown["MV"])))


# In[132]:


infos = {"bus":"BC","car":"CAR","van":"VAN","suv":"SUV","Wagon":"SWAGON","truck":"TRUCK"}
incident_time = {"morning":"MORN","noon":"NOON","afternoon":"AFTERN","evening":"EVENING","late":"LATE","early morning":"EMORN","unk":"UNK"}


# In[139]:


matcher = Matcher(nlp.vocab)
victims = ["boy","girl","daughter","son","twins","twin","children","child","babies","baby","kids","kid"]
death_tokens = ["die","kill","slaughter","murder","dead","death"]
rob_tokens = ['burglary','loot','rob','steal','theft','robbery','snatch']
drug_tokens = ['drug','remedy','antidote','panacea','nostrum','potion','elixir']
alcohol_tokens = ['alcohol','liquor','spirit','intoxicant','toxic']
make_tokens = list(filter(lambda x:type(x) != float ,cars["MAKE"]))
model_tokens = list(filter(lambda x:type(x) != float ,cars["MODEL"]))
loctwo_tokens = list(pairs.keys())
mvown_tokens = list(own.keys())
county_tokens = list(county["County"])
color_tokens = list(color["Colors"].apply(lambda x:x.lower()))
info_tokens = list(infos.keys())
Todtwo_tokens = list(incident_time.keys())
motoron_tokens = ["hit","strike","knock","crash"]


victim_pattern = [[{"POS":"NUM"},{"POS":"ADJ","OP": "?"},{"lemma": y}] for y in victims]
victim_pattern.extend([[{"TAG":"CD"},{"TAG":"NN"},{"TAG": "JJ"},{"lemma":r,"OP": "?"}] for r in ["boy","girl","daughter","son","twins","twin","children","child","babies","baby","kids","kid"]])
victim_pattern.extend([[{"LOWER":"toddler"},{"lemma":r}] for r in ["boy","girl","daughter","son","twin","children","child","babies","baby","kids","kid","die"] ])
death_pattern = [[{"LEMMA":d}] for d in death_tokens]
rob_pattern = [[{"LEMMA":r}] for r in rob_tokens]
drug_pattern = [[{"LEMMA":r}] for r in drug_tokens]
alcohol_pattern = [[{"LEMMA":r}] for r in alcohol_tokens]
make_pattern = [[{"lower":r.lower()}] for r in make_tokens]
model_pattern = [[{"lower":r.lower()}] for r in model_tokens]
loctwo_pattern = [[{"lower":r.lower()}] for r in loctwo_tokens]
howalone_into =[{"Lemma":"get"},{"lower":"into"}]
howalone_out =[{"Lemma":"get"},{"lower":"out"}]
howalone_left =[{"lemma":"leave"}]
howalone_play =[{"lemma":"play"}]
howalone_alone =[{"lemma":"alone"}]
color_pattern = [[{"lower":r.lower()}] for r in color_tokens]
Info_pattern = [[{"lower":r.lower()}] for r in info_tokens]
todtwo_pattern = [[{"lower":r.lower()}] for r in Todtwo_tokens]
restrain_pattern = [{"lower":"seat belt"}]
motoron_pattern = [[{"lower":r.lower()}] for r in motoron_tokens]
county_pattern = [[{"lower":r.lower()}] for r in county_tokens]
mvown_pattern = [[{"lower":r.lower()}] for r in mvown_tokens]

matcher.add("VICTIMS", None, *victim_pattern)
matcher.add("DEATHS", None, *death_pattern)
matcher.add("ROB", None, *rob_pattern)
matcher.add("DRUG", None, *drug_pattern)
matcher.add("ALCOHOL", None, *alcohol_pattern)
matcher.add("MAKE", None, *make_pattern)
matcher.add("MODEL", None, *model_pattern)
matcher.add("INFO",None,*Info_pattern)
matcher.add("LOCTWO",None, *loctwo_pattern)
matcher.add("TODTWO",None,*todtwo_pattern)
matcher.add("INTO",None, howalone_into)
matcher.add("OUT",None, howalone_out)
matcher.add("LEFT",None, howalone_left)
matcher.add("PLAY",None, howalone_play)
matcher.add("ALONE",None, howalone_alone)
matcher.add("COLOR",None,*color_pattern)
matcher.add("RESTRAIN",None,restrain_pattern)
matcher.add("MOTORON",None,*motoron_pattern)
matcher.add("COUNTY",None,*county_pattern)
matcher.add("OWN",None,*mvown_pattern)


# In[140]:


def howalone(model):
    matches = matcher(model)
    get_span = lambda x : next(filter(lambda y: y[0] == nlp.vocab.strings[x],matches),False)
    into = get_span("INTO")
    out = get_span("OUT")
    left = get_span("LEFT")
    play = get_span("PLAY")
    alone = get_span("ALONE")
    if into != False and len(into) > 0:
        Howalone = "GIOTO"
    elif(out != False and len(out) > 0 ):
        Howalone = "GOOTO"
    elif(left != False and len(left) > 0):
        Howalone = "LEFT"
    elif(play != False and len(play) > 0):
        Howalone = "PLAY"
    elif(alone != False and len(alone) > 0 ):
        Howalone = "ALONE"
    else:
        Howalone = "NOT" 
    return Howalone


# In[146]:



def get_dict(model):
    print("Executing - Rule Based Matcher")
    matches = matcher(model)
    get_span = lambda x : next(filter(lambda y: y[0] == nlp.vocab.strings[x],matches),False)
    vict = get_span("VICTIMS")
    make = get_span("MAKE")
    yea = model[make[1]-20:make[2]+20] if make else False
    mvYear = next(filter(lambda x: x.label_ == 'DATE',yea.ents),nlp("99")[0]) if yea else nlp("99")[0]
    mvYear = mvYear if str(mvYear).isdigit() else nlp("99")[0]
    mvMake = model[make[1]:make[2]] if make else nlp("UNK")[0]
    Model = get_span("MODEL")
    mvModel = model[Model[1]:Model[2]] if Model  else nlp("UNK")[0]
    loctwo = get_span("LOCTWO")
    LocTwo = model[loctwo[1]:loctwo[2]] if loctwo  else nlp("UNK")[0]
    color = get_span("COLOR")
    Mvcolor=model[color[1]:color[2]] if color  else nlp("UNK")[0]
    info = get_span("INFO")
    mvInfo=model[info[1]:info[2]] if info  else nlp("UNK")[0]
    tod = get_span("TODTWO")
    todTwo = model[tod[1]:tod[2]] if tod  else nlp("UNK")[0]
    restrain = get_span("RESTRAIN")
    motoron = get_span("MOTORON")
    Count = get_span("COUNTY")
    County = model[Count[1]:Count[2]][0] if Count else nlp("UNK")[0]
    mvown = get_span("OWN")
    Mvown = model[mvown[1]:mvown[2]] if mvown  else nlp("UNK")[0]
    
    
    
    print("Executing - Dependency Parser")
    if vict:
        tree = model[vict[1]:vict[2]]
        if("twin" in tree.lemma_):
            victim_count = nlp("2")[0]
        elif("trio" in tree.lemma_):
            victim_count = nlp("3")[0]
        else:
            victim_count = next(filter(lambda x: x.pos_ == "NUM",tree),nlp("0")[0])
            if(len(list(victim_count.ancestors)) > 0  and  list(victim_count.ancestors)[0].dep_ == 'npadvmod'):
                victim_count = nlp("1")[0]
# #             npadvmod
            
    else:
        victim_count = nlp("0")[0]
#     print("before head")
    victimmm = victim_count.head
    check_relative_intent = lambda xs,filt: any(list(map(lambda x: x == filt,xs.children)))
    howAlone = list(filter(lambda x : check_relative_intent(x,victimmm),victimmm.ancestors))
    howAlone = howAlone[0] if(len(howAlone) > 0) else nlp("LEFT")[0]
    

    inPlace = next(filter(lambda x : x.dep_ == "prep" and x.pos_ == "ADP",howAlone.subtree),False)
    inPlace = next(filter(lambda x : x.dep_ == "pobj",inPlace.children),False) if(inPlace) else nlp("UNK")[0]

#     mvMake = next(filter(lambda x : x.dep_ == "nmod",inPlace.children),False) if(inPlace) else nlp("UNK")[0]
#     mvColor = next(filter(lambda x : x.dep_ == "amod",inPlace.children),False) if(inPlace) else nlp("UNK")[0]
    
    if(inPlace):
        nearBy = next(filter(lambda x : x.dep_ == "prep" and x.pos_ == "ADP",inPlace.subtree),False)
        nearBy = next(filter(lambda x : x.dep_ == "pobj",nearBy.children),False) if(nearBy) else nlp("UNK")[0]
    else:
        nearBy = nlp("UNK")[0] 

    check_relative_dep = lambda xs,dep: list(filter(lambda x: x.dep_ == dep and x.pos_ == "NOUN",xs.children))
    haSPEC = list(map(lambda x : check_relative_dep(x,"nsubjpass"),victimmm.ancestors))
    haSPEC = next(filter(lambda x: len(x) > 0 ,haSPEC),[])
    haSPEC = haSPEC[0].text  if(len(haSPEC) > 0) else "UNK"
    haSPEC
    victim_count = w2n.word_to_num(victim_count.text) if str(victim_count).isalpha() else victim_count
    dea = get_span("DEATHS")
    is_death = "Y" if(dea) else "UNK"
    if dea:
        death_root = model[dea[1]:dea[2]][0]
        death_count = list(filter(lambda x : x.pos_ == "NUM",death_root.ancestors))
        death_count = death_count[0] if(len(death_count) > 0) else list(filter(lambda x : x.pos_ == "NUM",death_root.children))
        death_count = death_count[0] if(isinstance(death_count,list) and len(death_count) > 0) else (death_count if(isinstance(death_count,spacy.tokens.token.Token) and len(death_count) > 0) else 0)
        death_count = w2n.word_to_num(death_count.text) if str(death_count).isalpha() else death_count
        death_count = death_count + 1 if(not isinstance(death_count,spacy.tokens.token.Token) and death_count == 0 ) else death_count
    else:
        death_count = 0

    today = datetime.datetime(int("20"+(case_match.group('year'))),int(case_match.group('month')),int(case_match.group('day')))
    Wod = today.strftime('%A')[0:2].upper()
    
    caseHeader_nouns = list(list(model.sents)[0].noun_chunks)
    victimms = list(filter(lambda x : x.root.lemma_.lower() in victims,caseHeader_nouns))
    # print(list(victims[0].root.lefts)[0].pos_)
    # if(len(victims) > 0):
    #     victim_count = victims[0].root.left_edge if(victims[0].root.left_edge.pos_ == 'NUM') else 0
    # else: victim_count = 0
    print("Executing - Named Entity Recognizer")
    city = list(filter(lambda ent: ent.label_ == "GPE" and ent.text != "",model.ents))
    city = city[0].text if(len(city) > 0) else "UNK"
    temp = list(filter(lambda ent: ent.label_ == "QUANTITY",model.ents))
    temp = temp[0].text if(len(temp) > 0) else "UNK"
    loc =  list(filter(lambda ent: ent.label_ == "LOC",model.ents))
    loc = loc[0].text if(len(loc) > 0) else "UNK"
    rob = get_span("ROB")
    drug = get_span("DRUG")
    alcohol = get_span("ALCOHOL")
    data = {
        'SEQNUM':"1",
        'CASENUM':case_match.group('case'),
        'BATCH':"0",
        'SOURCE':"I",
        'MONTH':case_match.group('month'),
        'DAY':case_match.group('day'),
        'DAY OF WK':Wod,
        'YEAR':"20"+case_match.group('year'),
        "CITY":city,
        'ST':case_match.group('state'),
        'COUNTY':str(County),
        "VICTIMS":str(victim_count),
        "DEATH":is_death,
        "NUMDTHS":str(death_count),
        "NUMRES": "UNK",
        "MVOWN":own[str(Mvown).lower()] if str(Mvown)  in own.keys() else "Unknown",
        "MVMAKE":str(mvMake),
        "MVYEAR":str(mvYear),
        "MVMODEL":str(mvModel),
        "MVCOLOR":str(Mvcolor),
        "MVINFO":str(mvInfo).upper()+"1" if str(mvInfo) == "truck" else str(mvInfo).upper(),
        "MVADINFO":"UNK",
        "LOCKED":"UNK",
        "LOCATION":loc,
        "LOC_TWO": pairs[str(LocTwo).lower()] if str(LocTwo)  in pairs.keys() else "Unknown",
        "HOWALONE" :howalone(model),
        "haSPEC":haSPEC  + " " + howAlone.text,
        "RESTRAIN": "Y" if restrain != False else "N",
        "MOTORON":"Y" if motoron != False else "N",
        "KEYS":"Y" if motoron != False else "N",
        "INTENT":"UNK",
        "TYPE A":case_match.group("type_a"),
        "TYPE B": case_match.group("type_b"),
        "ROB": "Y" if(rob and len(rob) > 0) else "N",
        "DRUG": "Y" if(drug and len(drug) > 0) else "N",
        "ALCOHOL": "Y" if(alcohol and len(alcohol) > 0) else "N",
        "TOD":"UNK",
        "TOD TWO": incident_time[str(todTwo).lower()],
        "AOT":"999",
        "TEMP":str(temp) if "degree" in temp else 998,
        }
    return data


# In[147]:


def merge(x):
    if(len(x.value_counts()) > 1):
        if(x.value_counts().index[0] != "UNK" and x.value_counts().index[0] != "99" and x.value_counts().index[0] != "999" and x.value_counts().index[0] != "998" and str(x.value_counts().index[0]) != "0" and x.value_counts().index[0] !=  'Unknown'):
            value = x.value_counts().index[0]
        else:
            value = x.value_counts().index[1]
    elif(len(x.value_counts()) > 0):
        value = x.value_counts().index[0]
    else:
        value = ""
    return value


# In[148]:


paths_data = []
for root,dirs,files in os.walk("./docs/"):
    for file in files:
        paths_data.append(os.path.join(root,file))


# In[149]:


text = []
frames = []
for filepath in paths_data:
    try:
        print("Document "+ filepath)
        Now1 = datetime.datetime.now()
        startDt = Now1.strftime("%d/%m/%Y %H:%M:%S")
        print("Start Time - " + startDt)
        print("-------------Extracting entities-------------")
        lst = []
        docText = docx2txt.process(filepath)
        docText = docText.replace("-"," ")
        case_match = re.search("^\n?KC(?P<case>(?P<state>[A-Z]{2}).....) (?P<type_a>..) ?(?P<type_b>..)? (?P<month>[0-9]{1,2}) (?P<day>[0-9]{1,2}) (?P<year>[0-9]{1,2})",docText)
        matchesss = re.finditer("https?:\/\/.+",docText)
        check = list(matchesss)
        for i,te in enumerate(check):
            span = te.span()
            start = check[i-1].span()[1] if( i != 0) else 0
            if( i == len(check)-1):
                end = len(docText) - 1
            else: 
                end = span[0] if( i == 0) else check[i+1].span()[0]
                txt = docText[start:end]
                lst.append(get_dict(nlp(txt)))
                text.append(txt.replace("\n"," "))

            print("Finalizing - Document Output "+str(i))
        form =pd.DataFrame(lst)
        frames.append(form)
        print("Merging - Document Counts - "+ str(i))
        Now2 = datetime.datetime.now()
        timetaken = (Now2 - Now1).total_seconds()
        print("The Total timetaken is "+str(timetaken))
        print("\n")
    except:
        print("Error Document "+ filepath)
    


# In[152]:


finaldf = pd.concat(frames,axis=0,ignore_index=True)
w = finaldf.groupby("CASENUM").agg(lambda x: merge(x)).reset_index()
columns = ['SEQNUM','CASENUM','BATCH','SOURCE','MONTH','DAY','DAY OF WK','YEAR',"CITY",'ST','COUNTY',"VICTIMS","DEATH","NUMDTHS","MVOWN","MVMAKE","MVYEAR","MVMODEL","MVCOLOR","MVINFO","MVADINFO","LOCKED","LOCATION","LOC_TWO","HOWALONE" ,"haSPEC","RESTRAIN","MOTORON","KEYS","TYPE A","TYPE B","ROB","DRUG","ALCOHOL"]


# In[153]:


w[columns].to_excel("Datasheet.xlsx")


# In[ ]:





# In[ ]:




